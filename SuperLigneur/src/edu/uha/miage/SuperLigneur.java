/*
 * Creative commons CC BY-NC-SA 2020 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 *
 *     Adapt - You can remix, transform, and build upon the material
 *
 * Under the following terms :
 *
 *     Attribution - You must give appropriate credit, provide a link to the license,
 *     and indicate if changes were made. You may do so in any reasonable manner,
 *     but not in any way that suggests the licensor endorses you or your use.
 *
 *     NonCommercial — You may not use the material for commercial purposes.
 *
 *     ShareAlike — If you remix, transform, or build upon the material,
 *     you must distribute your contributions under the same license as the original.
 *
 * Notices:    You do not have to comply with the license for elements of
 *             the material in the public domain or where your use is permitted
 *             by an applicable exception or limitation.
 *
 * No warranties are given. The license may not give you all of the permissions
 * necessary for your intended use. For example, other rights such as publicity,
 * privacy, or moral rights may limit how you use the material.
 *
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */
package edu.uha.miage;

import caseine.tags.*;

/**
 * Classe de démonstration du tag @CodeQualityToCheck
 *
 * @author Yvan Maillot <yvan.maillot@uha.fr>
 */
@RelativeEvaluation
public class SuperLigneur {

    private int n;
    private char border, inside;

    /**
     * Affecte simplement et naturellement les attributs de même nom
     *
     * @param n
     * @param border
     * @param inside
     */
    @ToDoInConstructor("1.10")
    @CodeQualityToCheck(
            priority = 10,
            grade = 1,
            codeAnalyserMethodsName = "edu.uha.miage.SuperLigneurTest.codeQualityCheckForConstructorIntCharChar"
    )
    public SuperLigneur(int n, char border, char inside) {
        this.n = n;
        this.border = border;
        this.inside = inside;
    }

    /**
     * Affecte simplement et naturellement les attributs de même nom et donne à
     * inside la valeur ' '
     *
     * @param n
     * @param border
     */
    @ToDoInConstructor("1.20")
    @CodeQualityToCheck(
            priority = 20,
            grade = 1,
            codeAnalyserMethodsName = "edu.uha.miage.SuperLigneurTest.codeQualityCheckForConstructor"
    )

    public SuperLigneur(int n, char border) {
        this(n, border, ' ');
    }

    /**
     * Affecte simplement et naturellement l'attribut n et donne à border la
     * valeur '#' et à inside la valeur ' '
     *
     * @param n
     */
    @ToDoInConstructor("1.30")
    @CodeQualityToCheck(
            priority = 30,
            grade = 1,
            codeAnalyserMethodsName = "edu.uha.miage.SuperLigneurTest.codeQualityCheckForConstructor"
    )
    public SuperLigneur(int n) {
        this(n, '#', ' ');
    }

    /**
     * Donne
     *
     * à n la valeur 10
     *
     * à border la valeur '#'
     *
     * et à inside la valeur ' '
     *
     */
    @ToDoInConstructor("1.40")
    @CodeQualityToCheck(
            priority = 40,
            grade = 1,
            codeAnalyserMethodsName = "edu.uha.miage.SuperLigneurTest.codeQualityCheckForConstructor"
    )
    public SuperLigneur() {
        this(10);
    }

    /*

    /**
     * L'énoncé pourrait être
     *
     * -----------------------------------------------
     *
     * Écrire une méthode qui retourne une chaîne de caractères de taille n
     *
     * @param n la taille de la chaîne retournée
     * @param border de type char : pour le bord
     * @param inside de type char : pour l'intérieur
     * @return une chaîne de n caractères dont le premier et le dernier sont
     * "border" et les autres sont "inside", si n >= 2.
     *
     * La chaine vide, si n < 1
     *
     * Une chaine de 1 caractères qui est border, si n == 1
     *
     * -----------------------------------------------
     *
     * L'évaluation fonctionnelle est facile à mettre en oeuvre avec le tag
     * @ToCompare.
     *
     * -----------------------------------------------
     *
     * Mais même si la méthode écrite est fonctionnellement correcte, il est
     * possible que l'étudiant ne l'ait pas écrit
     * comme l'exige son enseignant.
     *
     * Par exemple, pour cet exercice, nous demandons que soit employée une
     * itération de type for et rien d'autre, car c'est la boucle la mieux
     * adaptée dans ce cas.
     *
     * Une erreur fréquente de nos débutants est d'écrire une boucle qui itère
     * de 0 à n-1 et de tester dans la boucle quand ajouter la bordure et quand
     * ajouter la partie intérieure. Par exemple ainsi :
     *
     * for (int i = 0; i < n; ++i) {
     *
     * if (i == 0 || i == n-1) line += border; else line += inside;
     *
     * }
     *
     * C'est évidemment très mauvais. Il faut l'éviter.
     *
     * Enfin, comme il est n'est pas performant de concaténer des String en Java
     * (en tout cas jusqu'à 1.8), il faut préférer StringBuilder.
     *
     * Le tag @CodeQualityToCheck permet d'aider à faire ces vérifications.
     *
     * Tel que déclaré ici, inutile d'expliquer les propriétés priority et
     * grade.
     *
     * requiersUnitTestsBefore = "1" signifie qu'on veut s'assurer que les tests
     * de priorité 1 (donc l'évaluation fonctionnelle) réussissent avant de
     * faire le test de qualité.
     *
     * codeAnalyserMethodsName =
     * "edu.uha.miage.UtilsTest.codeQualityCheckForALineWithABorder"
     *
     * Signifie que la méthode suivante doit existant dans la classe
     * edu.uha.miage.UtilsTest
     *
     * public static String codeQualityCheckForALineWithABorder(
     *
     * com.github.javaparser.ast.body.MethodDeclaration md
     *
     * )
     *
     * com.github.javaparser.ast.body.MethodDeclaration est une classe de l'api
     * javaparser et son instance contient un AST de la méthode annotée.
     * 
     * L'enseignant peut alors employer l'API de javaparser pour analyser la 
     * méthode de l'étudiant dans le détail.
     * 
     * https://javaparser.org/
     *
     */
    @ToDoIn("1.50")
    @ToCompare(priority = 50, grade = 2)
    @CodeQualityToCheck(
            priority = 60,
            grade = 4,
            requiersUnitTestsBefore = "50",
            codeAnalyserMethodsName = {
                "edu.uha.miage.SuperLigneurTest.codeQualityCheckForALineWithABorder",
                "edu.uha.miage.SuperLigneurTest.codeQualityCheckForALineWithABorderStringBuilder"}
    )
    public static String aLineWithABorder(int n, char border, char inside) {
        /* Pas ça
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            line.append((i == 0 || i == n - 1) ? border : inside);
        }
        return line.toString();
         */
        if (n <= 0) {
            return "";
        }
        if (n == 1) {
            return border + "";
        }

        StringBuilder s = new StringBuilder();
        s.append(border);
        for (int i = 0; i < n - 2; ++i) {
            s.append(inside);
        }
        s.append(border);
        return s.toString();
    }

}
