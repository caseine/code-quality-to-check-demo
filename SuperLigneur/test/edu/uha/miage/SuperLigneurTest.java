/*
 * Creative commons CC BY-NC-SA 2020 Yvan Maillot <yvan.maillot@uha.fr>
 *
 *     Share - You can copy and redistribute the material in any medium or format
 * 
 *     Adapt - You can remix, transform, and build upon the material 
 * 
 * Under the following terms :
 * 
 *     Attribution - You must give appropriate credit, provide a link to the license, 
 *     and indicate if changes were made. You may do so in any reasonable manner, 
 *     but not in any way that suggests the licensor endorses you or your use. 
 * 
 *     NonCommercial — You may not use the material for commercial purposes. 
 * 
 *     ShareAlike — If you remix, transform, or build upon the material, 
 *     you must distribute your contributions under the same license as the original. 
 * 
 * Notices:    You do not have to comply with the license for elements of 
 *             the material in the public domain or where your use is permitted 
 *             by an applicable exception or limitation. 
 * 
 * No warranties are given. The license may not give you all of the permissions 
 * necessary for your intended use. For example, other rights such as publicity, 
 * privacy, or moral rights may limit how you use the material. 
 * 
 * See <https://creativecommons.org/licenses/by-nc-sa/4.0/>.
 */
package edu.uha.miage;

import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.ConditionalExpr;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.SwitchExpr;
import com.github.javaparser.ast.stmt.*;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

/**
 *
 * @author Yvan Maillot <yvan.maillot@uha.fr>
 */
public class SuperLigneurTest {

    private static class Finder extends VoidVisitorAdapter<Void> {

        private int nbWhile = 0;
        private int nbDo = 0;
        private int nbFor = 0;
        private int nbForEach = 0;
        private int nbReturn = 0;
        private int nbIf = 0;
        private int nbString = 0;
        private int nbSwitch = 0;
        private int nbConditionalExpr = 0;
        private int nbExplicitConstructorInvocation = 0;
        private ForStmt theFor = null;
        private String theString = null;

        @Override
        public void visit(ExplicitConstructorInvocationStmt n, Void arg) {
            super.visit(n, arg);
            ++nbExplicitConstructorInvocation;
        }

        @Override
        public void visit(SwitchExpr n, Void arg) {
            super.visit(n, arg);
            ++nbSwitch;
        }

        @Override
        public void visit(ConditionalExpr n, Void arg) {
            super.visit(n, arg);
            ++nbConditionalExpr;
        }

        @Override
        public void visit(WhileStmt n, Void arg) {
            super.visit(n, arg);
            ++nbWhile;
        }

        @Override
        public void visit(ReturnStmt n, Void arg) {
            super.visit(n, arg);
            ++nbReturn;

        }

        @Override
        public void visit(IfStmt n, Void arg) {
            super.visit(n, arg);
            ++nbIf;
        }

        @Override
        public void visit(ForStmt n, Void arg) {
            super.visit(n, arg);
            theFor = n;
            ++nbFor;
        }

        @Override
        public void visit(ForEachStmt n, Void arg) {
            super.visit(n, arg);
            ++nbForEach;
        }

        @Override
        public void visit(DoStmt n, Void arg) {
            super.visit(n, arg);
            ++nbDo;
        }

        @Override
        public void visit(VariableDeclarator n, Void arg) {
            super.visit(n, arg);
            if (n.getTypeAsString().endsWith("String")) {
                ++nbString;
                theString = n.getNameAsString();
            }
        }

        @Override
        public void visit(MethodCallExpr n, Void arg) {
            super.visit(n, arg);
        }

        public int getNbWhile() {
            return nbWhile;
        }

        public int getNbFor() {
            return nbFor;
        }

        public int getNbDo() {
            return nbDo;
        }

        public int getNbForEach() {
            return nbForEach;
        }

        public int getNbReturn() {
            return nbReturn;
        }

        public int getNbIf() {
            return nbIf;
        }

        public int getNbString() {
            return nbString;
        }

        public int getNbExplicitConstructorInvocation() {
            return nbExplicitConstructorInvocation;
        }

        public ForStmt getTheFor() {
            return theFor;
        }

        public String getTheString() {
            return theString;
        }

        public int getNbSwitch() {
            return nbSwitch;
        }

        public int getNbConditionalExpr() {
            return nbConditionalExpr;
        }
    }

    public static String codeQualityCheckForConstructorIntCharChar(ConstructorDeclaration cd) {
        if (cd.getBody().getStatements().size() != 3) {
            return "Revoir le constructeur SuperLigneur(int, char, char) : 3 instructions sont nécessaires et suffisantes";
        }
        return "OK";
    }

    public static String codeQualityCheckForConstructor(ConstructorDeclaration cd) {
        Finder finder = new Finder();
        cd.accept(finder, null);
        if (finder.getNbExplicitConstructorInvocation() != 1) {
            return "Revoir vos constructeurs";
        }
        return "OK";
    }

    public static String codeQualityCheckForALineWithABorder(MethodDeclaration md) {
        Finder finder = new Finder();
        md.accept(finder, null);

        if (finder.getNbWhile() != 0) {
            return "Il ne devrait pas y avoir de \"while\"";
        }

        if (finder.getNbDo() != 0) {
            return "Il ne devrait pas y avoir de \"do\"";
        }

        if (finder.getNbForEach() != 0) {
            return "Il ne devrait pas y avoir de \"forEach\"";
        }

        if (finder.getNbFor() != 1) {
            return "Il devrait y avoir un et un seul \"for\"";
        }

        ForStmt forstmt = finder.getTheFor();

        Finder insideFinder = new Finder();
        forstmt.accept(insideFinder, null);
        if (insideFinder.getNbIf() != 0) {
            return "Il ne devrait pas y avoir de \"if\" dans votre boucle \"for\"";
        }
        if (insideFinder.getNbSwitch() != 0) {
            return "Il ne devrait pas y avoir de \"switch\" dans votre boucle \"for\"";
        }

        if (insideFinder.getNbConditionalExpr() != 0) {
            return "Il ne devrait pas y avoir d'opérateur ternaire (?:) dans votre boucle)";
        }
        return "OK";
    }

    public static String codeQualityCheckForALineWithABorderStringBuilder(MethodDeclaration md) {
        Finder finder = new Finder();
        md.accept(finder, null);
    

        if (finder.getNbString() != 0) {
            return "Il ne devrait pas y avoir de déclaration de String (pensez plutôt à utiliser StringBuilder)";
        }
        return "OK";
    }

}
